#! /usr/bin/env python3

from setuptools import setup
import re

# We probably ought to use debian.changelog, but let's avoid that
# dependency for now.
changelog_heading = re.compile(r"\w[-+0-9a-z.]* \(([^\(\) \t]+)\)")

with open("debian/changelog") as changelog:
    line = changelog.readline()
    match = changelog_heading.match(line)
    if match is None:
        raise ValueError("Failed to parse first line of debian/changelog: "
                         "'%s'" % line)
    version = match.group(1)

    # Make our version complies with PEP440.
    parsed = re.match(r'^([0-9]+(?:\.[0-9]+)*)(.*)$', version)
    if parsed is not None:
        pub_ver = parsed[1]
        local_ver = parsed[2]
        local_ver = re.sub(r'[^a-zA-Z0-9]+', '.', local_ver)
        local_ver = re.sub(r'^\.', '', local_ver)

        version = f'{pub_ver}+{local_ver}'

setup(
    name="apparmor.click",
    namespace_packages=['apparmor'],
    version=version,
    description="AppArmor click utility",
    author="Steve Beattie",
    author_email="steve.beattie@canonical.com",
    license="GNU GPL-2",
    scripts=['aa-clicktool', 'aa-clickhook', 'aa-clickquery'],
    # packages=['apparmor'],
    py_modules=['apparmor.click'],
    package_dir={'': 'src'},
)
