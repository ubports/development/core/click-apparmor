#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
# export DH_VERBOSE=1

PY3REQUESTED := $(shell py3versions -r)
PY3DEFAULT := $(shell py3versions -d)
# Run setup.py with the default python3 last so that the scripts use
# #!/usr/bin/python3 and not #!/usr/bin/python3.X.
PY3 := $(filter-out $(PY3DEFAULT),$(PY3REQUESTED)) python3
GNUTRIPLET := $(shell dpkg-architecture -qDEB_BUILD_MULTIARCH)

%:
	dh $@ --with python3,click

override_dh_auto_build:
	set -e; for python in $(PY3); do \
		$$python setup.py build; \
	done

override_dh_auto_install:
	# setuptools likes to leave some debris around, which confuses
	# things.
	find build -name __pycache__ -print0 | xargs -0r rm -rf
	find build -name \*.egg-info -print0 | xargs -0r rm -rf
	dh_auto_install
	set -e; for python in $(PY3); do \
		$$python setup.py install --force --root=$(CURDIR)/debian/tmp \
			--no-compile --install-layout=deb; \
	done
	# make location in /var to store symlinks
	mkdir -p $(CURDIR)/debian/tmp/var/lib/apparmor/clicks
	# copy aa-exec-click into place
	install -D -m 755 $(CURDIR)/aa-exec-click \
		$(CURDIR)/debian/tmp/usr/bin/aa-exec-click
	# adjust aa-exec-click for multiarch
	sed -i "s/gnutriplet='###GNUTRIPLET###'/gnutriplet='$(GNUTRIPLET)'/" \
		$(CURDIR)/debian/tmp/usr/bin/aa-exec-click

override_dh_click:
	dh_click --name apparmor
	dh_click --name apparmor-profile

override_dh_auto_clean:
	dh_auto_clean

override_dh_auto_test:
	#make pyflakes-check # needs locales setup
	make pep8-check
	LC_ALL=C make man-check
	make test
