TMPDIR   = ./tmp
PYFLAKES = $(TMPDIR)/pyflakes.out
PEP8 = $(TMPDIR)/pep8.out

ifndef $(PYTHON)
export PYTHON=python3
endif

PYFLAKES_EXE = pyflakes3

all:
	# Use setup.py to install. See README for details
	exit 1

test:
	$(PYTHON) ./test-clicktool.py

coverage:
	$(PYTHON) -m coverage run ./test-clicktool.py

coverage-report:
	$(PYTHON) -m coverage report --show-missing --omit="*/apparmor/easyprof.py,*/apparmor/common.py,test-clicktool.py,*/gi/*,*/pkg_resources.py"

pyflakes-check: clean
	$(shell test -d $(TMPDIR) || mkdir $(TMPDIR))
	$(shell $(PYFLAKES_EXE) ./src/apparmor/*py ./aa-clickhook ./aa-clicktool ./aa-clickquery ./test-clicktool.py 2>&1 | grep -v "undefined name '_'" > $(PYFLAKES))
	cat "$(PYFLAKES)"
	test ! -s "$(PYFLAKES)"
	@echo "Syntax is ok"

pep8-check: clean
	$(shell test -d $(TMPDIR) || mkdir $(TMPDIR))
	$(shell pycodestyle ./src/apparmor/*py ./aa-clickhook ./aa-clicktool ./aa-clickquery ./test-clicktool.py 2>&1 > $(PEP8))
	cat "$(PEP8)"
	test ! -s "$(PEP8)"
	@echo "PEP8 ok"

man-check: clean
	$(shell mkdir $(TMPDIR) 2>/dev/null)
	for page in `ls *.1`; do \
		manout=$(TMPDIR)/$$page.out; \
		echo "Checking $$page for errors... "; \
		PAGER=cat LANG='C' MANWIDTH=80 man --warnings -E ascii -l $$page >/dev/null 2> "$$manout"; \
		cat "$$manout"; \
		test ! -s "$$manout" || exit 1; \
		echo "PASS"; \
	done; \

	@echo "Man pages are ok"

check: pep8-check pyflakes-check man-check test

clean:
	rm -rf $(TMPDIR)
	rm -f ./src/apparmor/*.pyc
	rm -rf ./src/apparmor/__pycache__
	rm -rf ./__pycache__
	rm -rf ./.coverage
	rm -rf ./build
